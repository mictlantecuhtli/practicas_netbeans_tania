/*
Alumna: Fatima Azucena MC
Fecha: 26_11_2020
Correo: fatimaazucenamartinez274@gmail.com
 */
package com.mycompany.calculadora.programas_poo;
//Importacion de paquetes
import javax.swing.JOptionPane;
/*Realizar un programa el cual calcule el area y longitud de un circulo*/
public class Circunferencia {
    public static void main(String[] args){//Inicio metodo principal
        //Declaracion de variables
        double PI = 3.1416;
        double radio, area, longitud;
        
        //Entrada de datos por teclado
        radio = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el radio del circuenferencia: "));
        
        //Operaciones para sacar el area y longuitud
        longitud = 2 * PI * radio;
        area = PI * (radio*radio);
        
        //Salida de datos
        JOptionPane.showMessageDialog(null, "La longitud del circulo es de: "+longitud);
        JOptionPane.showMessageDialog(null, "El area del circulo: "+area);
    }//Fin metodo principal
}
