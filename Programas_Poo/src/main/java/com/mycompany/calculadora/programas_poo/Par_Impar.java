/*
Autor: Fatima Azucena MC
Fecha: 26_11_2020
Corre: fatimaazucenamartinez274@gmail.com
*/
package com.mycompany.calculadora.programas_poo;
//Importacion de paquetes
import javax.swing.JOptionPane;

/*Escriba un código en el cual se ingrese un numero, que saque el doble y
el triple de este y que indique si es par o impar*/
public class Par_Impar {//Inicico clase principal
    public static void main(String[] args){//Inicio metodo principal
            //Declaracion de variables
            int numero, numeroDoble, numeroTriple;
            
            //Entrada de datos por teclado
            numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero: "));
            
            //Operaciones para obtener el doble y el tripñe del numero ingresado
            numeroDoble = numero*2;
            numeroTriple = numero*3;
            
            if(numeroDoble%2 == 0){//Inicio condicional if-else_1
                JOptionPane.showMessageDialog(null, "El numero "+numeroDoble+" es doble de "+numero+ " y es par");
            } else{
                JOptionPane.showMessageDialog(null, "El numero "+numeroDoble+" es doble de "+numero+" y es impar");
            }//Fin condicional if-else_1
            
            if(numeroTriple%2 == 0){//Inicio if-else_2
                JOptionPane.showMessageDialog(null, "El numero "+ numeroTriple +" es triple de "+numero+" y es par");
            } else{
                JOptionPane.showMessageDialog(null, "El numero "+numeroTriple+" es trilpe de "+numero+" y es impar");
            }//Fin if-else_2   
    }//Fin metodo principal
}//Fin clase principal
