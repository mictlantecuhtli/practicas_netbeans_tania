/*
Alumna: Fatima Azucena MC
Fecha: 26_11_2020
Correo: fatimaazucenamartinez274@gmail.com
 */
package com.mycompany.calculadora.programas_poo;

//Importacion de paquetes
import javax.swing.JOptionPane;

/*Realice un programa el cual se ingrese un numero, si este es menor a 0 uqe
imprima un mensaje diciendo que es negativo, si es mayor a 100 que imprima un
mensaje diceindo que es mayor*/
public class Numero_Mayor {//Inicio clase principal
    public static void main(String [] args){//Inicio metodo principal
        //Declaracion de variables
        int numero;
        
        //Entrada de datos por teclado
        numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero: "));
        
        if (numero < 0){//Inicio condicional if-else-if
            JOptionPane.showMessageDialog(null, "El numero que usted a ingresado es negativo");
        } else if(numero >100){
            JOptionPane.showMessageDialog(null, "El numero que usted a ingresado es mayor a 100");
          }//Fin condicional if-else-if
    }//Fin metodo principal
}//Fin clase principal
