/*
Autor: Fatima Azucena MC
Fecha: 26_11_2020
Correo: fatimaazucenamartinez274@gmail.com
 */
package com.mycompany.calculadora.programas_poo;
//Importacion de paquetes
import javax.swing.JOptionPane; 

/*Escriba en codigo en Java el cual se ingresen grados centigrados y se
haga una conversion a grados Fharenheit*/

public class Conversion {//Inicio clase principal
   public static void main(String[] args){//Inicio metodo principal
       //Declaracion de variables
       int grados_Centigrados, grados_F;
       grados_Centigrados = Integer.parseInt(JOptionPane.showInputDialog("Ingrese los grados centigrados a convertir: "));
       grados_F = 32+(9-grados_Centigrados/5); 
       
       JOptionPane.showMessageDialog(null, "Los "+ grados_Centigrados+" grados centigrados equivale a "+grados_F+" grados Fharenheit");
   }//Fin metodo principal
}//Fin clase principal
