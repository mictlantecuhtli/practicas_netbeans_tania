/*
Autor: Fátima Azucena Martínez Cadena
Fecha: 26_11_2020
Correo: fatimaazucenamartinez274@gmail.com
 */
package com.mycompany.calculadora.programas_poo;

import javax.swing.JOptionPane;

public class Ordenar__Numeros {//Inicio clase principal
    public static void main(String[] args){//Inicio metodo principal
        //Declaracion de varibles
        int y, x, posicion;
        int numero[] = new int[3];
        
        //Entrada de datos por teclado
        numero[0] = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer numero: "));
        numero[1] = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo numero: "));
        numero[2] = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tercer numero: "));
        
        for (x = 0; x < 3; x++){//Inicio for_2
            for (y = 0; y < 2; y++){//Inicio for_2
                if (numero[y] > numero[y+1]){//Inicio condicional if
                    posicion = numero[y + 1];
                    numero[y+1] = numero[y];
                    numero[y] = posicion;
                }//Fin condicional if
            }//Fin for_3
        }//Fin for_1
        
        //Salida de datos
        JOptionPane.showMessageDialog(null, "El numero "+numero[2]+ " es el mayor de todos");
        JOptionPane.showMessageDialog(null, "El numero "+numero[1]+ " es el intermedio");
        JOptionPane.showMessageDialog(null, "El numero "+numero[0]+ " es el numero menor");
    }//Fin metodo principal
}//Fin clase principal
