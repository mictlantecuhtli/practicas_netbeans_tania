/*
Autor: Fatima Azucena MC
Fecha: 26_11_2020
Correo: fatimaazucenamartinez274@gmail.com
 */
package com.mycompany.calculadora.programas_poo;
//Importacion de paquetes
import javax.swing.JOptionPane;

/*Realice un programa que calcule el area de un triangulo*/
public class Area_Triangulo {//Inicio clase principal
    public static void main(String[] args){//Inicio metodo principal
        //Declaracion de variables
        double area, base, altura;
        
        //Entada de datos por teclado
        base = Double.parseDouble(JOptionPane.showInputDialog("Ingrese la base del triangulo: "));
        altura = Double.parseDouble(JOptionPane.showInputDialog("Ingrese la altura del triangulo: "));
        
        //Formula para sacar el area
        area = (base*altura)/2;
        
        //Salida de datos
        JOptionPane.showMessageDialog(null, "El area del triangulo es de: "+area+" cm");
    }//Fin metodo principal
}//Fin clase principal
